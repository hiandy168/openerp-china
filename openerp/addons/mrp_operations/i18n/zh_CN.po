# Chinese (Simplified) translation for openobject-addons
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the openobject-addons package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: openobject-addons\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-09-23 16:28+0000\n"
"PO-Revision-Date: 2014-09-30 12:30+0800\n"
"Last-Translator: 保定-粉刷匠 <992102498@qq.com>\n"
"Language-Team: Chinese (Simplified) <zh_CN@li.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2013-03-08 05:51+0000\n"
"X-Generator: Poedit 1.6.9\n"

#. module: mrp_operations
#: field:mrp.workorder,nbr:0
msgid "# of Lines"
msgstr "# 明细"

#. module: mrp_operations
#: help:mrp.production.workcenter.line,state:0
msgid ""
"* When a work order is created it is set in 'Draft' status.\n"
"* When user sets work order in start mode that time it will be set in 'In "
"Progress' status.\n"
"* When work order is in running mode, during that time if user wants to stop "
"or to make changes in order then can set in 'Pending' status.\n"
"* When the user cancels the work order it will be set in 'Canceled' status.\n"
"* When order is completely processed that time it is set in 'Finished' "
"status."
msgstr ""
"* 工单被创建时，“草稿“状态。\n"
"* 从工单被用户进入开始模式后，设为”在进行中“模式。\n"
"* 当工单是运行模式，在此期间，如果用户要停止或作出改变，可设置为”暂停“状"
"态。\n"
"* 当用户取消工单，设置为”取消状态“。\n"
"* 从工单被完成的时候开始，设置为”完成“状态。"

#. module: mrp_operations
#: model:ir.actions.act_window,help:mrp_operations.mrp_production_wc_action_planning
msgid ""
"<p class=\"oe_view_nocontent_create\">\n"
"            Click to start a new work order.\n"
"          </p><p>\n"
"            To manufacture or assemble products, and use raw materials and\n"
"            finished products you must also handle manufacturing "
"operations.\n"
"            Manufacturing operations are often called Work Orders. The "
"various\n"
"            operations will have different impacts on the costs of\n"
"            manufacturing and planning depending on the available workload.\n"
"          </p>\n"
"        "
msgstr ""
"<p class=\"oe_view_nocontent_create\">\n"
"            单击启动一个新的工单。\n"
"          </p><p>\n"
"            要生产或者组装产品，并且使用原料，也必须处理生产操作完成产品。\n"
"            生产操作经常被叫做工单。\n"
"            各种操作对生产成本有不同的影响，计划取决于可用的工作能力。\n"
"          </p>\n"
"        "

#. module: mrp_operations
#: model:ir.actions.act_window,help:mrp_operations.mrp_production_wc_action_form
msgid ""
"<p class=\"oe_view_nocontent_create\">\n"
"            Click to start a new work order. \n"
"          </p><p>\n"
"            Work Orders is the list of operations to be performed for each\n"
"            manufacturing order. Once you start the first work order of a\n"
"            manufacturing order, the manufacturing order is automatically\n"
"            marked as started. Once you finish the latest operation of a\n"
"            manufacturing order, the MO is automatically done and the "
"related\n"
"            products are produced.\n"
"          </p>\n"
"        "
msgstr ""
"<p class=\"oe_view_nocontent_create\">\n"
"            单击创建一个新的工单。\n"
"          </p><p>\n"
"            工单是每个生产单要被执行的操作列表。\n"
"            一旦你启动生产单的第一个工单，生产单将被自动标记为启动。\n"
"            一旦你完成了生产单最后一个操作，生产单被自动完成， \n"
"            相关的产品也被制造完成。\n"
"          </p>\n"
"        "

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.mrp_production_workcenter_form_view_inherit
msgid "Actual Production Date"
msgstr "实际生产日期"

#. module: mrp_operations
#: view:mrp_operations.operation:mrp_operations.operation_calendar_view
msgid "Calendar View"
msgstr "日历视图"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.mrp_production_workcenter_form_view_inherit
msgid "Cancel"
msgstr "取消"

#. module: mrp_operations
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view
msgid "Cancel Order"
msgstr "取消订单"

#. module: mrp_operations
#: selection:mrp.production.workcenter.line,production_state:0
msgid "Canceled"
msgstr "已作废"

#. module: mrp_operations
#: selection:mrp.production.workcenter.line,state:0
#: selection:mrp.workorder,state:0
#: selection:mrp_operations.operation.code,start_stop:0
msgid "Cancelled"
msgstr "已取消"

#. module: mrp_operations
#: help:mrp.production,allow_reorder:0
msgid ""
"Check this to be able to move independently all production orders, without "
"moving dependent ones."
msgstr "选中这里就不会按工作中心工作时间重排生产单日期。"

#. module: mrp_operations
#: field:stock.move,move_dest_id_lines:0
msgid "Children Moves"
msgstr "子调拨"

#. module: mrp_operations
#: field:mrp_operations.operation,code_id:0
#: field:mrp_operations.operation.code,code:0
msgid "Code"
msgstr "代码"

#. module: mrp_operations
#: model:ir.actions.act_window,name:mrp_operations.mrp_production_wc_confirm_action
msgid "Confirmed Work Orders"
msgstr "已确认的工单"

#. module: mrp_operations
#: field:mrp_operations.operation,create_uid:0
#: field:mrp_operations.operation.code,create_uid:0
msgid "Created by"
msgstr "创建人"

#. module: mrp_operations
#: field:mrp_operations.operation,create_date:0
#: field:mrp_operations.operation.code,create_date:0
msgid "Created on"
msgstr "创建在"

#. module: mrp_operations
#: view:mrp.workorder:mrp_operations.view_report_mrp_workorder_filter
msgid "Current"
msgstr "当前的"

#. module: mrp_operations
#: model:ir.filters,name:mrp_operations.filter_mrp_workorder_current_production
msgid "Current Production"
msgstr "当前产品"

#. module: mrp_operations
#: field:mrp.workorder,date:0
msgid "Date"
msgstr "日期"

#. module: mrp_operations
#: field:mrp.workorder,delay:0
msgid "Delay"
msgstr "延期"

#. module: mrp_operations
#: selection:mrp.production.workcenter.line,production_state:0
#: view:mrp.workorder:mrp_operations.view_report_mrp_workorder_filter
#: selection:mrp_operations.operation.code,start_stop:0
msgid "Done"
msgstr "已完成"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.view_mrp_production_workcenter_form_view_filter
#: selection:mrp.production.workcenter.line,production_state:0
#: selection:mrp.production.workcenter.line,state:0
#: selection:mrp.workorder,state:0
msgid "Draft"
msgstr "草稿"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.mrp_production_workcenter_form_view_inherit
msgid "Duration"
msgstr "时长"

#. module: mrp_operations
#: field:mrp.production.workcenter.line,date_finished:0
#: field:mrp.production.workcenter.line,date_planned_end:0
#: field:mrp_operations.operation,date_finished:0
msgid "End Date"
msgstr "结束日期"

#. module: mrp_operations
#: code:addons/mrp_operations/mrp_operations.py:122
#: code:addons/mrp_operations/mrp_operations.py:446
#: code:addons/mrp_operations/mrp_operations.py:450
#: code:addons/mrp_operations/mrp_operations.py:462
#: code:addons/mrp_operations/mrp_operations.py:465
#, python-format
msgid "Error!"
msgstr "错误！"

#. module: mrp_operations
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view
msgid "Finish Order"
msgstr "生产单完成"

#. module: mrp_operations
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view2
#: view:mrp.production.workcenter.line:mrp_operations.mrp_production_workcenter_form_view_inherit
#: selection:mrp.production.workcenter.line,state:0
#: selection:mrp.workorder,state:0
msgid "Finished"
msgstr "已完成"

#. module: mrp_operations
#: field:mrp.production,allow_reorder:0
msgid "Free Serialisation"
msgstr "自由排序生产"

#. module: mrp_operations
#: model:ir.actions.act_window,name:mrp_operations.mrp_production_wc_draft_action
msgid "Future Work Orders"
msgstr "待开工工单"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.view_mrp_production_workcenter_form_view_filter
#: view:mrp.workorder:mrp_operations.view_report_mrp_workorder_filter
msgid "Group By"
msgstr "分组按"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.graph_in_hrs_workcenter
msgid "Hours by Work Center"
msgstr "按工作中心工时合计"

#. module: mrp_operations
#: field:mrp.workorder,id:0 field:mrp_operations.operation,id:0
#: field:mrp_operations.operation.code,id:0
msgid "ID"
msgstr "ID"

#. module: mrp_operations
#: selection:mrp.production.workcenter.line,production_state:0
msgid "In Production"
msgstr "生产中"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.view_mrp_production_workcenter_form_view_filter
#: selection:mrp.production.workcenter.line,state:0
#: selection:mrp.workorder,state:0
msgid "In Progress"
msgstr "进行中"

#. module: mrp_operations
#: code:addons/mrp_operations/mrp_operations.py:455
#, python-format
msgid ""
"In order to Finish the operation, it must be in the Start or Resume state!"
msgstr "要完成工序，它必须处于开始或重新开始状态。"

#. module: mrp_operations
#: code:addons/mrp_operations/mrp_operations.py:446
#, python-format
msgid ""
"In order to Pause the operation, it must be in the Start or Resume state!"
msgstr "要暂停工序，必须在开始或重启状态。"

#. module: mrp_operations
#: code:addons/mrp_operations/mrp_operations.py:450
#, python-format
msgid "In order to Resume the operation, it must be in the Pause state!"
msgstr "要重启这个工序，必须在暂停状态。"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.mrp_production_workcenter_form_view_inherit
msgid "Information"
msgstr "信息"

#. module: mrp_operations
#: field:mrp_operations.operation,write_uid:0
#: field:mrp_operations.operation.code,write_uid:0
msgid "Last Updated by"
msgstr "最后更新被"

#. module: mrp_operations
#: field:mrp_operations.operation,write_date:0
#: field:mrp_operations.operation.code,write_date:0
msgid "Last Updated on"
msgstr "最后更新在"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.view_mrp_production_workcenter_form_view_filter
msgid "Late"
msgstr "延迟"

#. module: mrp_operations
#: model:ir.model,name:mrp_operations.model_mrp_production
msgid "Manufacturing Order"
msgstr "生产单"

#. module: mrp_operations
#: code:addons/mrp_operations/mrp_operations.py:122
#, python-format
msgid "Manufacturing order cannot be started in state \"%s\"!"
msgstr "处于\"%s\"状态的生产单不能开始！"

#. module: mrp_operations
#: view:mrp.workorder:mrp_operations.view_report_mrp_workorder_filter
msgid "Month Planned"
msgstr "计划月份"

#. module: mrp_operations
#: code:addons/mrp_operations/mrp_operations.py:462
#, python-format
msgid "No operation to cancel."
msgstr "没有操作被取消"

#. module: mrp_operations
#: model:ir.actions.act_window,name:mrp_operations.mrp_production_code_action
msgid "Operation Codes"
msgstr "工序代码"

#. module: mrp_operations
#: field:mrp_operations.operation.code,name:0
msgid "Operation Name"
msgstr "工序名称"

#. module: mrp_operations
#: code:addons/mrp_operations/mrp_operations.py:442
#, python-format
msgid ""
"Operation has already started! You can either Pause/Finish/Cancel the "
"operation."
msgstr "操作已经开始！你可以 暂停/完成/取消 这个操作。"

#. module: mrp_operations
#: code:addons/mrp_operations/mrp_operations.py:458
#, python-format
msgid "Operation is Already Cancelled!"
msgstr "工序已经取消"

#. module: mrp_operations
#: code:addons/mrp_operations/mrp_operations.py:465
#, python-format
msgid "Operation is already finished!"
msgstr "工序已完成"

#. module: mrp_operations
#: code:addons/mrp_operations/mrp_operations.py:435
#, python-format
msgid "Operation is not started yet!"
msgstr "工序还未开始！"

#. module: mrp_operations
#: model:ir.actions.act_window,name:mrp_operations.mrp_production_operation_action
#: view:mrp.production.workcenter.line:mrp_operations.workcenter_line_calendar
#: view:mrp.production.workcenter.line:mrp_operations.workcenter_line_gantt
msgid "Operations"
msgstr "工序"

#. module: mrp_operations
#: field:mrp_operations.operation,order_date:0
msgid "Order Date"
msgstr "单据日期"

#. module: mrp_operations
#: selection:mrp.workorder,state:0
#: selection:mrp_operations.operation.code,start_stop:0
msgid "Pause"
msgstr "暂停"

#. module: mrp_operations
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view
msgid "Pause Work Order"
msgstr "暂停工单"

#. module: mrp_operations
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view2
#: view:mrp.production.workcenter.line:mrp_operations.mrp_production_workcenter_form_view_inherit
#: view:mrp.production.workcenter.line:mrp_operations.view_mrp_production_workcenter_form_view_filter
#: selection:mrp.production.workcenter.line,state:0
msgid "Pending"
msgstr "等待中"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.mrp_production_workcenter_form_view_inherit
msgid "Planned Date"
msgstr "计划日期"

#. module: mrp_operations
#: view:mrp.workorder:mrp_operations.view_report_mrp_workorder_filter
msgid "Planned Month"
msgstr "计划月份"

#. module: mrp_operations
#: field:mrp.production.workcenter.line,product:0
#: view:mrp.workorder:mrp_operations.view_report_mrp_workorder_filter
#: field:mrp.workorder,product_id:0
msgid "Product"
msgstr "产品"

#. module: mrp_operations
#: field:mrp.workorder,product_qty:0
msgid "Product Qty"
msgstr "产品数量"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.mrp_production_workcenter_form_view_inherit
msgid "Product to Produce"
msgstr "待生产的产品"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.view_mrp_production_workcenter_form_view_filter
#: view:mrp.workorder:mrp_operations.view_report_mrp_workorder_filter
#: field:mrp.workorder,production_id:0
#: field:mrp_operations.operation,production_id:0
msgid "Production"
msgstr "生产"

#. module: mrp_operations
#: view:mrp_operations.operation:mrp_operations.mrp_production_operation_tree_view
msgid "Production Operation"
msgstr "生产工序"

#. module: mrp_operations
#: view:mrp_operations.operation.code:mrp_operations.mrp_production_code_form_view
#: view:mrp_operations.operation.code:mrp_operations.mrp_production_code_tree_view
msgid "Production Operation Code"
msgstr "工序代码"

#. module: mrp_operations
#: field:mrp.production.workcenter.line,production_state:0
msgid "Production Status"
msgstr "生产状态"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.mrp_production_workcenter_form_view_inherit
msgid "Production Workcenter"
msgstr "生成工作中心"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.view_mrp_production_workcenter_form_view_filter
msgid "Production started late"
msgstr "未准时开始生产"

#. module: mrp_operations
#: field:mrp.production.workcenter.line,qty:0
msgid "Qty"
msgstr "数量"

#. module: mrp_operations
#: model:ir.filters,name:mrp_operations.filter_mrp_workorder_quantity_produced
msgid "Quantity Produced"
msgstr "已生产的产品"

#. module: mrp_operations
#: selection:mrp.production.workcenter.line,production_state:0
msgid "Ready to Produce"
msgstr "生产准备就绪"

#. module: mrp_operations
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view2
#: view:mrp.production.workcenter.line:mrp_operations.mrp_production_workcenter_form_view_inherit
#: selection:mrp_operations.operation.code,start_stop:0
msgid "Resume"
msgstr "重新开始"

#. module: mrp_operations
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view
msgid "Resume Work Order"
msgstr "重启工单"

#. module: mrp_operations
#: field:mrp.production.workcenter.line,date_planned:0
msgid "Scheduled Date"
msgstr "计划日期"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.view_mrp_production_workcenter_form_view_filter
msgid "Scheduled Date by Month"
msgstr "按月的计划日期"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.view_mrp_production_workcenter_form_view_filter
msgid "Scheduled Month"
msgstr "计划月份"

#. module: mrp_operations
#: view:mrp.workorder:mrp_operations.view_report_mrp_workorder_filter
msgid "Search"
msgstr "查询"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.view_mrp_production_workcenter_form_view_filter
msgid "Search Work Orders"
msgstr "查找工单："

#. module: mrp_operations
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view2
#: view:mrp.production.workcenter.line:mrp_operations.mrp_production_workcenter_form_view_inherit
msgid "Set Draft"
msgstr "设为草稿"

#. module: mrp_operations
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view
msgid "Set to Draft"
msgstr "设为草稿"

#. module: mrp_operations
#: code:addons/mrp_operations/mrp_operations.py:435
#: code:addons/mrp_operations/mrp_operations.py:442
#: code:addons/mrp_operations/mrp_operations.py:455
#: code:addons/mrp_operations/mrp_operations.py:458
#, python-format
msgid "Sorry!"
msgstr "抱歉!"

#. module: mrp_operations
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view2
#: view:mrp.production.workcenter.line:mrp_operations.mrp_production_workcenter_form_view_inherit
#: selection:mrp_operations.operation.code,start_stop:0
msgid "Start"
msgstr "开始"

#. module: mrp_operations
#: field:mrp.production.workcenter.line,date_start:0
#: field:mrp_operations.operation,date_start:0
msgid "Start Date"
msgstr "开始日期"

#. module: mrp_operations
#: view:mrp.production:mrp_operations.mrp_production_form_inherit_view
msgid "Start Working"
msgstr "开始工作"

#. module: mrp_operations
#: model:ir.actions.report.xml,name:mrp_operations.report_code_barcode
msgid "Start/Stop Barcode"
msgstr "开始/停止 条码"

#. module: mrp_operations
#: view:mrp.workorder:mrp_operations.view_report_mrp_workorder_filter
msgid "Started"
msgstr "开始"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.view_mrp_production_workcenter_form_view_filter
#: field:mrp.production.workcenter.line,state:0
#: view:mrp.workorder:mrp_operations.view_report_mrp_workorder_filter
#: field:mrp.workorder,state:0
#: field:mrp_operations.operation.code,start_stop:0
msgid "Status"
msgstr "状态"

#. module: mrp_operations
#: model:ir.model,name:mrp_operations.model_stock_move
msgid "Stock Move"
msgstr "库存调拨"

#. module: mrp_operations
#: help:mrp.production.workcenter.line,delay:0
msgid "The elapsed time between operation start and stop in this Work Center"
msgstr "工序在这个工作中心从开始到结束的执行周期。"

#. module: mrp_operations
#: field:mrp.workorder,total_cycles:0
msgid "Total Cycles"
msgstr "总加工次数"

#. module: mrp_operations
#: field:mrp.workorder,total_hours:0
msgid "Total Hours"
msgstr "总小时数"

#. module: mrp_operations
#: field:mrp.production.workcenter.line,uom:0
msgid "Unit of Measure"
msgstr "计量单位"

#. module: mrp_operations
#: selection:mrp.production.workcenter.line,production_state:0
msgid "Waiting Goods"
msgstr "等待原料"

#. module: mrp_operations
#: view:mrp.production.workcenter.line:mrp_operations.view_mrp_production_workcenter_form_view_filter
#: view:mrp.workorder:mrp_operations.view_report_mrp_workorder_filter
#: field:mrp.workorder,workcenter_id:0
#: field:mrp_operations.operation,workcenter_id:0
msgid "Work Center"
msgstr "工作中心"

#. module: mrp_operations
#: model:ir.actions.act_window,name:mrp_operations.mrp_production_wc_resource_planning
msgid "Work Centers"
msgstr "工作中心"

#. module: mrp_operations
#: model:ir.actions.report.xml,name:mrp_operations.report_wc_barcode
msgid "Work Centers Barcode"
msgstr "工作中心条码"

#. module: mrp_operations
#: model:ir.actions.act_window,name:mrp_operations.action_report_mrp_workorder
#: model:ir.model,name:mrp_operations.model_mrp_production_workcenter_line
msgid "Work Order"
msgstr "工单"

#. module: mrp_operations
#: model:ir.ui.menu,name:mrp_operations.menu_report_mrp_workorders_tree
msgid "Work Order Analysis"
msgstr "工单分析"

#. module: mrp_operations
#: model:ir.model,name:mrp_operations.model_mrp_workorder
msgid "Work Order Report"
msgstr "工单报表"

#. module: mrp_operations
#: model:ir.actions.act_window,name:mrp_operations.mrp_production_wc_action_form
#: model:ir.ui.menu,name:mrp_operations.menu_mrp_production_wc_order
#: view:mrp.production.workcenter.line:mrp_operations.mrp_production_workcenter_form_view_inherit
#: view:mrp.production.workcenter.line:mrp_operations.mrp_production_workcenter_tree_view_inherit
#: view:mrp.production.workcenter.line:mrp_operations.view_mrp_production_workcenter_form_view_filter
#: view:mrp.workorder:mrp_operations.view_report_mrp_workorder_graph
msgid "Work Orders"
msgstr "工单"

#. module: mrp_operations
#: model:ir.ui.menu,name:mrp_operations.menu_mrp_production_wc_action_planning
msgid "Work Orders By Resource"
msgstr "工单（按资源）"

#. module: mrp_operations
#: model:ir.actions.act_window,name:mrp_operations.mrp_production_wc_action_planning
msgid "Work Orders Planning"
msgstr "工单计划"

#. module: mrp_operations
#: field:mrp.production.workcenter.line,delay:0
msgid "Working Hours"
msgstr "工时(小时)"

#. module: mrp_operations
#: model:ir.filters,name:mrp_operations.filter_mrp_workorder_workload
msgid "Workload"
msgstr "工作量"

#~ msgid "Date Planned (month)"
#~ msgstr "计划月份（月）"

#~ msgid "Date Planned (year)"
#~ msgstr "计划年度（年）"

#~ msgid "Planned Day"
#~ msgstr "计划日期"

#~ msgid "Planned Year"
#~ msgstr "计划年度"

#~ msgid "mrp_operations.operation"
#~ msgstr "mrp_operations.operation"

#~ msgid "mrp_operations.operation.code"
#~ msgstr "mrp_operations.operation.code"

#~ msgid "Cancel the operation."
#~ msgstr "取消工序"

#~ msgid "Creation of the work order"
#~ msgstr "新建工单"

#~ msgid "Details of the work order"
#~ msgstr "工单明细"

#~ msgid "Finish the operation."
#~ msgstr "完成工序"

#~ msgid "Information from the production order."
#~ msgstr "来自生产单的信息"

#~ msgid "Information from the routing definition."
#~ msgstr "信息来自工艺定义"

#~ msgid "Mrp Operations"
#~ msgstr "工序"

#~ msgid "Operation Cancelled"
#~ msgstr "工序已作取消"

#~ msgid "Operation Done"
#~ msgstr "工序完成"

#~ msgid "Operation cancelled"
#~ msgstr "工序已作废"

#~ msgid "Operation done"
#~ msgstr "工序完成"

#~ msgid "Picking Exception"
#~ msgstr "领料异常"

#~ msgid "Production Order"
#~ msgstr "生产单"

#~ msgid "Start Operation"
#~ msgstr "开始工序"

#~ msgid "Start the operation."
#~ msgstr "开始这工序"

#~ msgid "The work orders are created on the basis of the production order."
#~ msgstr "工单基于生产单创建"

#~ msgid ""
#~ "There is 1 work order per work center. The information about the number "
#~ "of cycles or the cycle time."
#~ msgstr "每个加工中心有一个工单。有加工的次数或工时数"

#~ msgid ""
#~ "When the operation is finished, the operator updates the system by "
#~ "finishing the work order."
#~ msgstr "在工序完成后，操作者更新在系统信息已完成工单。"

#~ msgid ""
#~ "When the operation needs to be cancelled, you can do it in the work order "
#~ "form."
#~ msgstr "可以在工序界面作废这样工序"

#~ msgid "March"
#~ msgstr "3月"

#~ msgid "Day"
#~ msgstr "天"

#~ msgid "July"
#~ msgstr "7月"

#~ msgid "September"
#~ msgstr "9月"

#~ msgid "December"
#~ msgstr "12月"

#~ msgid "Month"
#~ msgstr "月"

#~ msgid "August"
#~ msgstr "8月"

#~ msgid "June"
#~ msgstr "6月"

#~ msgid "November"
#~ msgstr "11月"

#~ msgid "October"
#~ msgstr "10月"

#~ msgid "January"
#~ msgstr "1月"

#~ msgid "May"
#~ msgstr "5月"

#~ msgid "February"
#~ msgstr "2月"

#~ msgid "April"
#~ msgstr "4月"

#~ msgid "#Line Orders"
#~ msgstr "明细"

#~ msgid "Year"
#~ msgstr "年"
